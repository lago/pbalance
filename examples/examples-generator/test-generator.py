#!/usr/bin/python3

import random

# This is probably not very efficient (we should
# generate a list once), but that's ok.
def numlines():
    tmp = random.choices([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
            weights=[1,1,1,4,4,4,4,4,3,3,3,2,1,1,1], k=1)
    return tmp[0]

def numfootnotelines():
    tmp = random.choices([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
            weights=[9,7,5,3,1,1,1,1,1,1,1,1,1,1,1], k=1)
    return tmp[0]

def floattype():
    tmp = random.choices(['figure', 'table'], weights=[5, 2], k=1)
    return tmp[0]

def floatpos():
    tmp = random.choices(
        ['h', 't', 'b', 'p', 'ht', 'hb', 'hp', 'htb', 'htbp', 'tp', 'tbp'],
        weights=[50, 10, 1, 1, 400, 10, 10, 600, 800, 10, 20], k=1)
    return tmp[0]

def floatorpar(numfloats, numpars):
    tmp = random.choices(['float', 'par'], weights=[numfloats, numpars], k=1)
    return tmp[0]

def printsection():
    if random.random() < 0.12: # 12% probability
        print("\\section{Some section}")

def printfootnote():
    if random.random() < 0.02: # 2% probability
        print("\\footnote{", end="")
        print("This is the beginning of a meaningless footnote.", end="")
        i = numfootnotelines() -1
        while i > 0:
            print("\\\\")
            print("Some meaningless footnote text.", end="")
            i -= 1
        print("}", end="")

def printpar():
    printsection()
    print("This is the beginning of a meaningless paragraph.", end="")
    i = numlines() -1
    while i > 0:
        print("\\\\")
        print("Some meaningless text.", end="")
        printfootnote()
        i -= 1
    print("\\par")


thefloat = "\
\\begin{{{0}}}[{1}]\n\
  \\centering\n\
  \\fbox{{%\n\
    \\parbox[c][{2}\\baselineskip][c]{{.9\\columnwidth}}\n\
    {{\\hfill This is a {0}\\hfill\\null}}%\n\
  }}\n\
\\end{{{0}}}"

def printfloat():
    tmp = 10 * random.random() # from 0.0 to 10.0
    tmp = tmp // 1 # truncate
    tmp = tmp / 10 # from 0.0 to 1.0 with only one decimal place
    print(thefloat.format(floattype(), floatpos(), numlines() + tmp))

preamble="\
\\documentclass[twocolumn]{article}\n\n\
\\flushbottom\n\n\
\\usepackage{pbalance}\n\n\
\\renewcommand{\\topfraction}{.85}\n\
\\renewcommand{\\textfraction}{.15}\n\
\\renewcommand{\\floatpagefraction}{.75}\n\
\\renewcommand{\\dblfloatpagefraction}{.66}\n\
\\setcounter{topnumber}{8}\n\
\\setcounter{dbltopnumber}{3}\n\
\\setcounter{bottomnumber}{8}\n\
\\setcounter{totalnumber}{20}\n\
\\begin{document}\n"

###########

numpars = random.randint(0,12)

numfloats = random.choices([0,1,2,3,4,5,6,7], weights=[2,10,14,8,6,4,2,1], k=1)
numfloats = numfloats[0]

print(preamble)

# Always start with a paragraph
printpar()
print()

while numpars > 0 or numfloats > 0:
    what = floatorpar(numfloats, numpars)
    if what == 'float':
        printfloat()
        print()
        numfloats -= 1
    else:
        printpar()
        print()
        numpars -= 1

print("\\end{document}")
