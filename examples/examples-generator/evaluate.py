#!/usr/bin/python

import re
#import shutil

with open('experiment.aux') as f:
    contents = f.read()

match = re.search('toggle(.{4,5}) \{@PBimpossible\}', contents)
impossible = match.group(1)

if impossible == "false":

    match = re.search('@PBunbalReallyLastPage\{([^}]*)\}', contents)
    lastunbal = match.group(1)
    
    match = re.search('@PBbalReallyLastPage\{([^}]*)\}', contents)
    lastbal = match.group(1)
    
    match = re.search('@PBunbalpg.{1,5}UsedLeft\}\{([^}]*)pt\}', contents)
    unbalLeft = match.group(1)
    match = re.search('@PBunbalpg.{1,5}UsedRight\}\{([^}]*)pt\}', contents)
    unbalRight = match.group(1)
    
    match = re.search('@PBbalpg.{1,5}UsedLeft\}\{([^}]*)pt\}', contents)
    balLeft = match.group(1)
    match = re.search('@PBbalpg.{1,5}UsedRight\}\{([^}]*)pt\}', contents)
    balRight = match.group(1)
    
    if lastunbal < lastbal:
        #shutil.move('experiment.tex', 'addedpages')
        exit(1)
else:
    exit(2)
