#!/bin/bash

cnt=0
id=0

mkdir -p crashes
mkdir -p others
mkdir -p impossible

function run_experiment {

    ./test-generator.py > experiment.tex

    latexmk experiment

    result=$?

    if [ $result -gt 0 ]; then
        mv experiment.tex  crashes/experiment$id.tex
        id=$(( $id + 1 ))
    else
        ./evaluate.py
        result=$?

        case $result in
        0)
          :
          ;;
        1)
          id=$(( $id + 1 ))
          mv experiment.tex addedpages/experiment$id.tex
          ;;
        2)
          mv experiment.tex impossible/experiment$id.tex
          ;;
        *)
          exit 1
          ;;
        esac
    fi
}

while [ $cnt -lt 1000 ]; do
    rm -rf experiment.*
    run_experiment
    cnt=$(( $cnt + 1 ))
    rm -rf experiment.*
done
