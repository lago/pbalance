This is (1) a couple of scripts that can generate synthetic LaTeX documents,
compile them and check for some inconsistencies, and (2) a bunch of LaTeX
documents that exercise the features of the package.
