system("pdflatex pbalance.ins");

@default_files = ('pbalance.dtx');

set_tex_cmds('-synctex=1 -halt-on-error %O %S');

$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;

$silent = 1;
$silence_logfile_warnings = 1;

# Make latexmk -c/-C clean *all* generated files
$cleanup_includes_generated = 1;
$cleanup_includes_cusdep_generated = 1;
$bibtex_use = 2;

add_cus_dep('glo', 'gls', 0, 'genHistory');
sub genHistory{
  return system("makeindex -s gglo.ist -o pbalance.gls pbalance.glo");
}

push @generated_exts, 'glo', 'gls', 'sty';
