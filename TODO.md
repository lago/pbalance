# TODO

 * It would be nice to add the package option `final` so that balancing
   is activated even if the user passed `draft` to the class

 * Since 2020/10/01, LaTeX implements a new hooks system (in souce2e.pdf,
   check the section "lthooks.dtx"). In the future (after TeXLive 2024,
   debian bookworm and ubuntu 24.04 are released and arxiv uses at least
   TeXLive 2021), we should change the code to use the new LaTeX hooks:

   - `atbegshi` is now emulated by `atbegshi-ltx`; we can remove it and
     substitute `\AtBeginShipout{code}` with `\AddToHook{shipout/before}{code}`
     (see `texdoc ltshipout-doc`)
      
   - `atveryend` is now emulated by `atveryend-ltx`; we can remove it
     and substitute `\AfterLastShipout{code}` with
     `\AddToHook{enddocument/afterlastpage}{code}` (see `texdoc source2e`
     in the section about compatibility hooks such as `\AtBeginDocument`
     and `\AtEndDocument`).

   - the resources of the `filehook` package are now wrappers around the
     kernel macros; we can remove it and substitute `\AtEndOfPackageFile*`
     with `\AddToHook{package/pkgname/after}` (see `texdoc ltfilehook-doc`).
     Unfortunately, there is no replacement for `\AtEndOfPackageFile`.

 * Bug: if addtocurcol is overwritten, the package still works. However,
   we will fail to take float heights into account, which means the
   results could be bad.
