# Compatibility with other packages

## Packages that work fine

 - stfloats: we have code to handle it

 - memoir: we have code to handle it

 - footmisc: we have code to handle it

 - ftnright: we have code to handle it

 - flafter: we have code to handle it

 - fnpos: we have code to handle it

 - midfloat, cuted: we have code to handle them

 - mparhack: redefines @addmarginpar and @outputdblcol, but the new versions
   call the old ones, so there are no problems with pbalance

 - pdfcolfoot: modifies @makecol, but with a patch, so our patches are not
   overwritten. Actually, it patches several known variations of @makecol
   too.

 - ltugboat.cls: redefines @outputdblcol, but we are almost certainly
   going to be called after it, so things work ok

 - dblfnote: assumes the document starts in one-column mode; issuing
   \twocolumn at the beginning of the document solves any issues

## Packages that (probably) do not work

 - changebar modifies @makecol, the new version calls the old one. However,
   it modifies the @outputbox in a way that causes an error with pbalance:
   @PBcollectColumnUsedHeight tries to "open" the box, which causes an
   "infinite glue" error. It should be possible to make a specific patch
   to cb@makecol that grabs the box before it is changed and repeat the
   call \let\@makecol\cb@makecol. Still, since this package is mostly useful
   during document preparation, this incompatibility is a minor issue

 - balance, flushend, ltxgrid, revtex, paracol, flowfram, etiketka,
   slides: it makes no sense to use pbalance with any of these

 - fix2col: is obsolete since 2015

 - pxftnright, pxstfloats: japanese versions of ftnright and stfloats;
   no idea how to test these

 - refart, refrep, marginfix: no support for two-column mode

 - altxext: part of arabart.cls. No idea how to test it

 - nidanfloat: apparently, is abandoned. Also, I believe stfloats provides
   the same functionality

 - jacow: redefines @outputdblcol, but could work because we are always
   loaded after it. However, it also loads flushend

 - ledmac, eledmac, reledmac: I haven't even tried, but it is very unlikely
   that these will work and be useful

 - fnpara: footmisc is probably a better option

 - footbib: seems to be abandoned; biblatex is probably a better option

 - footmisx: this is a fork of footmisc that is essentially deprecated

## Packages that we should examine more closely

 - jpsj

 - aguplus and iagproc
